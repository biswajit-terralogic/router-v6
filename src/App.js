import React from "react";
import { Route, Outlet, Routes, useNavigate, Link } from "react-router-dom";

import "./App.css";

const App = () => {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="contact-us" element={<ContactUs />} />
        <Route path="path" element={<Path />}>
          <Route index element={<PathDetails />} />
          <Route path="life" element={<PathLife />} />
          <Route path="life/details" element={<LifeDetails />} />
        </Route>
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </div>
  );
};

const Home = () => <div>(Hi I am Home)</div>;

const About = () => <div>(Hi I am About)</div>;

const ContactUs = () => <div>(Hi I am ContactUs)</div>;

const Path = () => (
  <div>
    <Outlet />
  </div>
);

const PathDetails = () => {
  const router = useNavigate();

  return (
    <div>
      (Hi, I am Path details page.{" "}
      <span
        onClick={() => router("/path/life")}
        style={{ color: "red", cursor: "pointer" }}
      >
        Click me
      </span>{" "}
      to visit life path.)
    </div>
  );
};

const PathLife = () => {
  const router = useNavigate();
  return (
    <div>
      (Hello, I am Path Life page.{" "}
      <span
        onClick={() => router("/path/life/details")}
        style={{ color: "red", cursor: "pointer" }}
      >
        Click me
      </span>{" "}
      to visit life details page.)
      <Outlet />
    </div>
  );
};

const LifeDetails = () => <div>(Hi, I am everything about life.)</div>;

const NotFoundPage = () => <div>(404, Not found)</div>;

const Header = () => (
  <nav className="header">
    <Link to="/">
      <span style={{ cursor: "pointer" }}>Home</span>
    </Link>

    <Link to="/about">
      <span style={{ cursor: "pointer" }}>About</span>
    </Link>
    <Link to="/contact-us">
      <span style={{ cursor: "pointer" }}>Contact us</span>
    </Link>
    <Link to="/path">
      <span style={{ cursor: "pointer" }}>Path</span>
    </Link>
    <Link to="/path/life">
      <span style={{ cursor: "pointer" }}>Life</span>
    </Link>
  </nav>
);

export default App;
